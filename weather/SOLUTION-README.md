#¿Qué has empezado implementando y por qué?

Lo primero que he realizado ha sido modificar la clase de test proporcionada, en esta clase se encontraba un método que únicamente mostraba por consola el resultado de la llamada, lo cual era inservible para realizar test automáticos. 


Me ha parecido importante tener este punto solucionado para tener ya un test que valide que la aplicación funciona, ya que se trata de una refactorización me ha servido a modo de regresivo, durante el desarrollo.


También así he asentado una metodología TDD donde cada nuevo método creaba un test antes de realizarlo de manera que fallase, y posteriormente podía pasar  tanto el test unitario, como el regresivo del método principal para validar tanto los métodos concretos como el global.

#¿Qué problemas te has encontrado al implementar los tests y cómo los has solventado?

Mi principal duda a la hora de afrontar los test  ha sido que tecnología usar. En un primer momento me he decantado por **Mockito**, pero tras analizar al envergadura del ejercicio he decidido mantener Junit ya que me ha parecido suficiente y adecuado para cubrir las pruebas necesarias.


En cuanto a la implementación de los test, los he dividido por método, intentando no sobrecargar con test repetitivos, pero cubrir toda la funcionalidad.

#¿Qué componentes has creado y por qué?

A pesar de ser una aplicación sencilla, he creo importante diferenciar bien cada funcionalidad dentro de ella, por lo cual he creado diversos componentes:


- **MetaWeatherClient** proporciona un cliente limpio para realizar las llamadas a **MetaWeather**, donde he evitado el duplicado de código que había en el programa inicial, y a su vez he encapsulado en diversos métodos toda la funcionalidad necesaria para que quede transparente al método principal. 


- **ConsolidatedWeatherDTO**, **LocationDTO** y **SearchResultDTO** forman la capa de transporte, ya que me ha parecido importante tener esta capa claramente definida sustituyendo así la búsqueda de palabras claves en el json directamente. Esto hace que el proceso sea mucho más mantenible si a futuro necesitamos que el servicio nos proporcione más datos, o necesitamos modificar una llamada.


- **WeatherValidator** es un validador para los datos de entrada del servicio, nuevamente aunque parezcan pocos datos, requieren una serie de validaciones, como verificar si la ciudad llega vacía, o si la fecha es correcta y viene informada, estas validaciones no se pueden dejar en el cuerpo del método principal, ya que dificultarían la lectura del mismo. 
Además podrían necesitarse estas validaciones en algún sub-módulo a futuro, por lo cual conviene tenerlas encapsuladas en un Validador. 


- **WeatherError** y **WeatherException**, me ha parecido importante crear una excepción y un enumerado de errores controlados, ya que esta información será la que pueda llegar al API y éste podrá así generar un error controlado para mostrar al cliente basado en esta información.


#Si has utilizado dependencias externas, ¿por qué has escogido esas dependencias?

Solo he utilizado dos dependencias externas:


- **commons-lang3** (org.apache.commons), realmente no habría sido necesaria, pero me ha ofrecido funcionalidades como String.isNotEmpty, que permiten dejar el código más limpio.


- **Jackson-databind** (com.fasterxml.jackson.core), en este caso me ha parecido importante, para transformar el mensaje json a objeto Java y trabajar de forma más clara con él. Mediante el ObjectMapper de dicha dependencia se permite una configuración que hace cómoda esta lectura. En este caso por poner un ejemplo me ha permitido leer las propiedades del servicio (en snake case) de forma directa contra mi objeto java de transporte (en camel case).

#¿Has utilizado streams, lambdas y optionals de Java 8? ¿Qué te parece la programación funcional?

Sí, he usado un stream con lambda, para recorrer el array de resultados hasta encontrar uno valido.  Esto me ha devuelto un Optional del cual he extraído el resultado.


Se puede encontrar este fragmento en **MetaWeatherClient** -> getWeatherForSpecificdate().
Me parece que la programación funcional que nos ofrece Java 8 es fantástica para muchas situaciones donde de no usarla deberíamos hacer un código más largo y a veces más ineficiente para realizar la misma lógica.


#¿Qué piensas del rendimiento de la aplicación? 

La aplicación es muy sencilla, y su rendimiento dependerá en gran medida de la API externa que utiliza. Eso sí no está preparada para afrontar una gran carga de peticiones por segundo.

#¿Qué harías para mejorar el rendimiento si esta aplicación fuera a recibir al menos 100 peticiones por segundo?

Es una aplicación que no usa datos propios, ni necesita caches ni tiene grandes flecos donde buscar optimizaciones, a diferencia de lo que pasa con por ejemplo con los sistemas de consultas de disponibilidad.


Si fuera a recibir un número considerable de peticiones por segundo se podría mejorar el rendimiento usando un sistema distribuido donde tuviéramos la aplicación levantada en diferentes maquinas o dockers, y las peticiones se balancearían mediante alguna tecnología round-robin o similar. También podríamos hacer uso de Threads. 


Un punto, eso sí que me parece importante, es que en este servicio dependemos de una API externa de forma muy directa, sería interesante estudiar cómo responde esta plataforma ante un gran aumento de llamadas por segundo, para asegurarnos que no hará de cuello de botella y nuestro gasto en optimización de rendimiento se verá mermado por ello.


#¿Cuánto tiempo has invertido para implementar la solución? 

Si hablamos puramente de la solución técnica, algo más de dos horas. He dedicado también un tiempo a responder este documento que no contabilizo en esas dos horas, y algo más a documentar un poco el java doc. 
En total lo he hecho todo durante esta tarde del viernes con pequeñas pausas, pero sin espaciarlo en diferentes días.

#¿Crees que en un escenario real valdría la pena dedicar tiempo a realizar esta refactorización?

Absolutamente. Habría que encontrar el hueco, no siempre se puede dedicar tiempo a refactorización cuando temas prioritarios aprietan y por ello es extremadamente importante imponer unas bases de buenas prácticas al inicio de un proyecto. 


Sin embargo creo que es muy importante sacar huecos en épocas de menos carga para refactorizar partes de código que se crean necesarias. De lo contrario cuando toca mantener este código, se perderá más tiempo en ello, y en global habremos perdido más tiempo.


Sobre todo (y sin decir que el resto no sean importantes) si estamos siguiendo una metodología basada en test, tener partes del código sin ellos, puede ser muy perjudicial a futuro, por lo cual veo necesario invertir algo de tiempo en ello.


#Aclaraciones personales

Me gustaría aclarar algunos puntos sobre mi solución


- **Sobre el uso de logs** He estado a punto e añadir unos logs, pero finalmente he optado por no hacerlo ya que creo que el punto correcto donde añadirlos sería en la API de entrada, y no en el módulo proporcionado. Este módulo ya enviara a la API las excepciones e información necesaria para ser registrada en los logs. También sería interesante (según el interés que pueda haber en la aplicación), crear unos logs de acceso, logeando cada llamadas a la APi, lo cual nos daría una información estadística interesante. Quizás esta información podría almacenar en un MongoDB.


- **Sobre el endpoint** He considerado que es un endpoint único, por lo cual lo he dejado HardCodeado, al igual que el número de días que permitimos la previsión. Sin embargo lo más adecuado acabaría siendo sacar estos dos datos a un properties, ya que pueden variar por entorno o petición de negocio.


- **Sobre la fecha de entrada** Además de validar que la fecha no sea superior a seis días, he validado que no sea anterior al día de hoy. Esto en un entorno real convendría confirmarlo, sin embargo en esta situación me ha parecido lógico no permitir una fecha anterior a hoy para una previsión del tiempo. (A pesar de que he confirmado que la API lo permite)


- **Sobre la API** (nota anecdótica) he revisado la API para ver si podríamos ahorrarnos el bucle por días, pero la única funcionalidad que ofrecía pedir por día nos devolvía una infinidad de datos, en función de su timestamp para ese día, por lo cual he tenido que mantenerlo en forma de stream.








