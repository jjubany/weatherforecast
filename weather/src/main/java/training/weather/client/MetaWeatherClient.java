package training.weather.client;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;

import training.weather.dto.ConsolidatedWeatherDTO;
import training.weather.dto.LocationDTO;
import training.weather.dto.SearchResultDTO;

/**
 * Client for any interactions with MetaWeather API
 */
public class MetaWeatherClient {

	private static final String REQUIRED_DATE_FORMAT = "yyyy-MM-dd";
	private static final String ENDPOINT = "https://www.metaweather.com/api/location/";
	private static final String SEARCH_QUERY = "search/?query=";

	private static ObjectMapper objectMapper;

	public MetaWeatherClient() {
		objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
	}

	/**
	 * Get the API WhereOnEarth id for a specific city
	 */
	public String getWhereOnEarthId(String city) throws Exception {

		String jsonResponse = callHttpService(SEARCH_QUERY + city);

		SearchResultDTO[] searchResults = objectMapper.readValue(jsonResponse, SearchResultDTO[].class);

		return searchResults[0].getWoeid();
	}

	/**
	 * Get the weather forecast given a location id
	 */
	public String getWeatherForSpecificDate(String whereOnEarthId, Date date) throws Exception {

		String jsonResponse = callHttpService(whereOnEarthId);

		LocationDTO location = objectMapper.readValue(jsonResponse, LocationDTO.class);

		String providedDate = parseDateIntoRequiredFormat(date);

		ConsolidatedWeatherDTO consolidatedWeather = Arrays.stream(location.getConsolidatedWeather())
				.filter(x -> x.getApplicableDate().equals(providedDate)).findFirst().get();

		return consolidatedWeather.getWeatherStateName();
	}

	/**
	 * Generic HTTPS call to MEtaWeather ENDPOINT
	 */
	private String callHttpService(String extension) throws Exception {
		HttpRequestFactory requestFactory = new NetHttpTransport().createRequestFactory();
		HttpRequest request = requestFactory.buildGetRequest(new GenericUrl(ENDPOINT + extension));
		return request.execute().parseAsString();
	}

	/**
	 * Format date into required string format
	 */
	private String parseDateIntoRequiredFormat(Date date) {
		if (date != null) {
			return new SimpleDateFormat(REQUIRED_DATE_FORMAT).format(date);
		}
		return null;
	}

}
