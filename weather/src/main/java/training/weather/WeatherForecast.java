package training.weather;

import java.util.Date;

import training.weather.client.MetaWeatherClient;
import training.weather.validate.WeatherError;
import training.weather.validate.WeatherException;
import training.weather.validate.WeatherValidator;

/**
 * Weather prediction utilities
 */
public class WeatherForecast {

	private static MetaWeatherClient metaWeatherClient = new MetaWeatherClient();

	/**
	 * Method that provides the weather forecast for a future (up to six days) date
	 */
	public String getCityWeather(String city, Date dateTime) throws WeatherException {

		try {
			city = WeatherValidator.validateCity(city);
			dateTime = WeatherValidator.validateDate(dateTime);

			String whereOnEarthId = metaWeatherClient.getWhereOnEarthId(city);
			return metaWeatherClient.getWeatherForSpecificDate(whereOnEarthId, dateTime);

		} catch (WeatherException weatherException) {
			throw weatherException;
		} catch (Exception exception) {
			WeatherException weatherException = new WeatherException(WeatherError.UNEXPECTED_ERROR, exception);
			throw weatherException;
		}

	}
}
