package training.weather.validate;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * Weather Forecast Request data validation tools
 */
public class WeatherValidator {

	private static final int MAX_FORECAST_DAYS = 6;

	/**
	 * Check a valid city input
	 */
	public static String validateCity(String city) throws WeatherException {

		if (StringUtils.isNotEmpty(city)) {
			return city;
		}

		throw new WeatherException(WeatherError.INVALID_INPUT_CITY);
	}

	/**
	 * Check a valid date input
	 */
	public static Date validateDate(Date date) throws WeatherException {
		if (date == null) {
			return new Date();
		}

		if (isValidDateRange(date)) {
			return date;
		}

		throw new WeatherException(WeatherError.INVALID_INPUT_DATE);
	}

	/**
	 * Check if a date its into an acceptable time lapse
	 */
	private static boolean isValidDateRange(Date date) {

		Calendar today = Calendar.getInstance();

		Calendar inSixDays = Calendar.getInstance();
		inSixDays.add(Calendar.DAY_OF_YEAR, MAX_FORECAST_DAYS);

		if (date.before(today.getTime()) || date.after(inSixDays.getTime())) {
			return false;
		}

		return true;

	}
}
