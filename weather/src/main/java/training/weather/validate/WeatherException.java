package training.weather.validate;

/**
 * Weather Forecast Custom Exception
 */
public class WeatherException extends Exception {

	private static final long serialVersionUID = -3337887968411042929L;

	private WeatherError error;

	public WeatherException(WeatherError error) {
		this.error = error;
	}

	public WeatherException(WeatherError error, Throwable cause) {
		super(cause);
		this.error = error;
	}

	@Override
	public String getMessage() {
		return error.getError();
	}

}
