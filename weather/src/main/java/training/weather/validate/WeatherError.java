package training.weather.validate;

/**
 * WeatherForecat Error List
 */
public enum WeatherError {

	INVALID_INPUT_CITY("ERR01: You must provide a valid city"),
	INVALID_INPUT_DATE("ERR02: You can only have a forecast from today to six days"),
	UNEXPECTED_ERROR("ERR99: Unexpected error");

	private String error;

	private WeatherError(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}

}
