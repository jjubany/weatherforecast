package training.weather.dto;

/**
 * Transport Object to receive the information of a Meta Weather location info
 */
public class LocationDTO {

	private ConsolidatedWeatherDTO[] consolidatedWeather;

	public ConsolidatedWeatherDTO[] getConsolidatedWeather() {
		return consolidatedWeather;
	}

	public void setConsolidatedWeather(ConsolidatedWeatherDTO[] consolidatedWeather) {
		this.consolidatedWeather = consolidatedWeather;
	}

}
