package training.weather.dto;

/**
 * Transport Object to receive the information of a Meta Weather search
 */
public class SearchResultDTO {

	private String title;
	private String woeid;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWoeid() {
		return woeid;
	}

	public void setWoeid(String woeid) {
		this.woeid = woeid;
	}

}
