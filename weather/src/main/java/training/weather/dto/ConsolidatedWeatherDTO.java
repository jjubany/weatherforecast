package training.weather.dto;

/**
 * Transport Object to receive the information of a Meta Weather
 * consolidatedWeather info
 */
public class ConsolidatedWeatherDTO {

	private String weatherStateName;

	private String applicableDate;

	public String getWeatherStateName() {
		return weatherStateName;
	}

	public void setWeatherStateName(String weatherStateName) {
		this.weatherStateName = weatherStateName;
	}

	public String getApplicableDate() {
		return applicableDate;
	}

	public void setApplicableDate(String applicableDate) {
		this.applicableDate = applicableDate;
	}

}
