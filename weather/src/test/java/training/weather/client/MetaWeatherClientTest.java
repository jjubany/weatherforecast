package training.weather.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Date;

import org.junit.Test;

public class MetaWeatherClientTest {

	MetaWeatherClient client = new MetaWeatherClient();

	@Test
	public void getWhereOnEarthIdTest() throws Exception {
		assertEquals(client.getWhereOnEarthId("Madrid"), "766273");
	}
	
	@Test
	public void getWeatherForSpecificDateTest() throws Exception {
		assertFalse(client.getWeatherForSpecificDate("766273", new Date()).isEmpty());
	}
}
