package training.weather.validate;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

public class WeatherValidatorTest {

	@Test
	public void validateCityTest() throws WeatherException {
		WeatherValidator.validateCity("Madrid");
	}

	@Test(expected = WeatherException.class)
	public void validateCityEmptyTest() throws WeatherException {
		WeatherValidator.validateCity("");
	}

	@Test
	public void validateDateTest() throws WeatherException {
		assertEquals(WeatherValidator.validateDate(new Date()), new Date());
		assertEquals(WeatherValidator.validateDate(null), new Date());
	}

	@Test(expected = WeatherException.class)
	public void validateDateOneMonthLaterTest() throws WeatherException {
		Calendar testDate = Calendar.getInstance();
		testDate.add(Calendar.MONTH, 1);
		WeatherValidator.validateDate(testDate.getTime());
	}
	
	@Test(expected = WeatherException.class)
	public void validateDateOneYesterdayTest() throws WeatherException {
		Calendar testDate = Calendar.getInstance();
		testDate.add(Calendar.MONTH, 1);
		WeatherValidator.validateDate(testDate.getTime());
	}

}
