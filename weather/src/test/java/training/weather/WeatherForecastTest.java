package training.weather;

import static org.junit.Assert.assertFalse;

import java.util.Date;

import org.junit.Test;

public class WeatherForecastTest {

	private WeatherForecast weatherForecast = new WeatherForecast();

	@Test
	public void getCityWeatherWithDateTest() throws Exception {
		assertFalse(weatherForecast.getCityWeather("Madrid", new Date()).isEmpty());
	}
	
	@Test
	public void getCityWeatherWithoutDateTest() throws Exception {
		assertFalse(weatherForecast.getCityWeather("Madrid", null).isEmpty());
	}
}